//
//  MapViewController.swift
//  MyPlaces
//
//  Created by Tarabanov Igor on 27.03.2020.
//  Copyright © 2020 Tarabanov Igor. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

protocol MapViewControllerDelegate {
    func getAddress(_ address: String?)
}

class MapViewController: UIViewController {
    
    var mapViewControllerDelegate: MapViewControllerDelegate?
    var place = Place()
    
    let annotationIdentifier = "annotationIdentifier"
    let locationManager = CLLocationManager()
    let locationDistance = 1000.00
    var incomeSegueIdentifier = ""
    var placeCoordinate: CLLocationCoordinate2D?
    var directionsArray: [MKDirections] = [] // массив построеныъ маршрутов нужен для того чтоб убирать с карты старые маршруты
    var previousLocation: CLLocation? { //свойство для хранения предыдущего местоположения пользователя
        didSet{
            startTrackingUserLocation()
        }
    }
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var currentAdresslabel: UILabel!
    @IBOutlet weak var pinImageView: UIImageView!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var goButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMapView()
        currentAdresslabel.text = ""
        mapView.delegate = self
        checkLoactionServices()
    }
    @IBAction func closeVC() {
        dismiss(animated: true)
    }
    @IBAction func showUserInCenterLocation() {
        showUserLocation()
    }
    @IBAction func buttonDoneAction() {
        mapViewControllerDelegate?.getAddress(currentAdresslabel.text)
        dismiss(animated: true)
    }
    @IBAction func goButtonPressed() {
        getDirections()
    }
    
    private func setupMapView(){
        goButton.isHidden = true
        
        if incomeSegueIdentifier == "showPlace"{
            setupPlacemark()
            currentAdresslabel.isHidden = true
            pinImageView.isHidden = true
            buttonDone.isHidden = true
            goButton.isHidden = false
        }
    }
    
    private func resetMapView(withNew directions: MKDirections){
        
        //удаляем текущие маршруты
        mapView.removeOverlays(mapView.overlays)
        //массив маршуротов который мы берем из параметров нашего метода
        directionsArray.append(directions)
        let _ = directionsArray.map { $0.cancel() }
        directionsArray.removeAll()
    }
    
    
    private func setupPlacemark(){
        guard let location = place.location else {return}
        
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location) { (placemarks, error) in
            
            if let error = error{
                print(error)
                return
            }
            
            let placemark = placemarks?.first
            
            let annotation = MKPointAnnotation()
            annotation.title = self.place.name
            annotation.subtitle = self.place.type
            
            guard let placemarkLocation = placemark?.location else {return}
            
            annotation.coordinate = placemarkLocation.coordinate
            self.placeCoordinate = placemarkLocation.coordinate
            
            self.mapView.showAnnotations([annotation], animated: true)
            self.mapView.selectAnnotation(annotation, animated: true)
        }
    }
    
    private func checkLoactionServices(){
        if CLLocationManager.locationServicesEnabled(){
            setupLocationManager()
            checkLocationAuthorization()
            
        }else{
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.alertMessage(title: "Title", message: "Message")
                }
            }
        }
    private func setupLocationManager(){
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
    }
    
    private func checkLocationAuthorization(){
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways:
            break
        case .authorizedWhenInUse:
            mapView.showsUserLocation = true
            if incomeSegueIdentifier == "getAdress" { showUserLocation() }
            break
        case .denied:
            //show alert controller
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.alertMessage(title: "You must give access", message: "Change location status at settings")
                }
            break
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
            break
        case .restricted:
            break
        @unknown default:
            print("swift add new case")
        }
    }
    
    private func alertMessage(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    private func getDirections(){
        //определяем текущее местоположение
        guard let location = locationManager.location?.coordinate else {
            self.alertMessage(title: "Error", message: "cant find your location")
            return
        }
        //включаем постоянное отслеживание местоположения пользователя
        locationManager.startUpdatingLocation()
        //передаем текущее местоположения пользователя
        previousLocation = CLLocation(latitude: location.latitude, longitude: location.longitude)
        
        //запрос на прокладку маршрута
        guard let request = createDirectionsRequest(from: location) else {
            self.alertMessage(title: "Error", message: "destanation is not found (неудаеться проложить маршрут)")
            return
        }
        //если все прошло успешно то создаем маршрут на основе тех сведений которые имеються в запросе
        let directions = MKDirections(request: request)
        
        //удаляем все старые маршруты перед построением новых
        resetMapView(withNew: directions)
        
        //запкскаем расчет маршрутов, метод calculate возвращает расчитанный маршрут со всеми данными
        directions.calculate { (response, error) in
            //пробуем извлечь ошибку
            if let error = error {
                print(error)
                return
            }
            //если ошибки не оказалось то пробуем извлечь обработаный маршрут
            guard let response = response else {
                self.alertMessage(title: "Error", message: "Direction is not avalible")
                return
            }
            //обьект response содержит в себе массив routes c маршрутами
            for route in response.routes {
                //MKRoute каждый из возможных наборов маршрутов для пользователя содержит в себе:
                                //ожидаемое время в пути
                                //дистанцию
                                // и все уведевления о поездке, и другое
                //создаем на карте наложение со всеми дополнительными маршрутами
                self.mapView.addOverlay(route.polyline) //содержит в себе подробную геометрию всего маршрута
                //фокусируем карту чтоб было видно весь маршрут от А до Б
                self.mapView.setVisibleMapRect(route.polyline.boundingMapRect, animated: true)
                
                //работа с дополнительной информацией расстояние и время в пути
                let distance = String(format: "%.1f", route.distance / 1000)//округляем до десятых и переводим метры в километры
                let timeInterval = route.expectedTravelTime
                
                
                print("Расстояние до обьекта: \(distance) км.")
                print("Время в пути: \(timeInterval) сек.")
            }
        }
    }
    //настройка метода на прокладку маршрута
    private func createDirectionsRequest(from coordinate: CLLocationCoordinate2D) -> MKDirections.Request?{
        //передаем координаты которые мы уже определили в месте когда указали пин на карте с мастом заведения
        guard let destinationCoordinate = placeCoordinate else {return nil}
        //определяем место начала маршрута которое в данном случае зависит от местоположения пользователя
        let startingLocation = MKPlacemark(coordinate: coordinate)
        //определяем место назначения (destinationCoordinate это все лиш коордиканаты с помощью которых мы сможем определить место на карте)
        let destanation = MKPlacemark(coordinate: destinationCoordinate)
        
        //запрос на построения маршрута от точки А к точкм Б
        let request = MKDirections.Request()
        //начало маршрута
        request.source = MKMapItem(placemark: startingLocation)
        //конец маршрута
        request.destination = MKMapItem(placemark: destanation)
        //задаем тип транспорта
        request.transportType = .automobile
        //разрешаем построение альтернативных маршрутов
        request.requestsAlternateRoutes = true
        
        return request
    }
    
    private func getCenterLocation(for mapView: MKMapView) -> CLLocation{
        let longitude = mapView.centerCoordinate.longitude
        let latitude = mapView.centerCoordinate.latitude
        
        return CLLocation(latitude: latitude, longitude: longitude)
    }
    
    private func showUserLocation(){
        if let location = locationManager.location?.coordinate{
                 let region = MKCoordinateRegion(center: location,
                                                 latitudinalMeters: locationDistance,
                                                 longitudinalMeters: locationDistance)
                 mapView.setRegion(region, animated: true)
             }
    }
    
    private func startTrackingUserLocation(){
        //проверяем что prevousLocation != nil (точка где находится пользователь)
        guard let previousLocation = previousLocation else { return }
        //координаты центра отображаемой области
        let center = getCenterLocation(for: mapView)
        
        //проверяем если дистанция между двумя этими точками изменилась на более 50 то мы перерисовываем маршрут иначе возврат из метода
        guard center.distance(from: previousLocation) > 50 else { return }
        //задаем новые координаты если дистанция изменилась на >50
        self.previousLocation = center
        
        //позиционируем карту с текущем положением пользователя но делаем с задержкой иначе карта сфокусируеться сразу
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.showUserLocation()
        }
    }
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard !(annotation is MKUserLocation) else { return nil}
        
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.canShowCallout = true
            
        }
        
        if let imageData = place.imageData{
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            imageView.layer.cornerRadius = 10
            imageView.clipsToBounds = true
            imageView.image = UIImage(data: imageData)
            annotationView?.rightCalloutAccessoryView = imageView
        }
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        
        let center = getCenterLocation(for: mapView)
        let geocoder = CLGeocoder()
        
        if incomeSegueIdentifier == "showPlace" && previousLocation != nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.showUserLocation()
            }
        }
        
        geocoder.cancelGeocode()
        
        geocoder.reverseGeocodeLocation(center) { (placemarks, error) in
            
            if let error = error{
                print(error)
                return
            }
            
            guard let placemarks = placemarks else { return }
            
            let placemark = placemarks.first
            let streetName = placemark?.thoroughfare
            let buildName = placemark?.subThoroughfare
            
            DispatchQueue.main.async {
                if streetName != nil && buildName != nil {
                    self.currentAdresslabel.text = "\(streetName!), \(buildName!)"
                }else if streetName != nil{
                    self.currentAdresslabel.text = "\(streetName!)"
                }else{
                    self.currentAdresslabel.text = ""
                }
            }
        }
    }
    //метод отвечает за покраску маршрута
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        
        let renderer = MKPolylineRenderer(overlay: overlay as! MKPolyline)
        renderer.strokeColor = .blue
        
        return renderer
    }
}
extension MapViewController: CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAuthorization()
    }
}
