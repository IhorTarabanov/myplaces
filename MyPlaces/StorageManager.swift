//
//  StorageManager.swift
//  MyPlaces
//
//  Created by Tarabanov Igor on 25.03.2020.
//  Copyright © 2020 Tarabanov Igor. All rights reserved.
//

import RealmSwift

let realm = try! Realm()

class StorageManager{
    
    static func saveObject(_ place: Place){
        try! realm.write{
            realm.add(place)
        }
    }
    
    static func removeObject(_ place: Place){
        try! realm.write{
            realm.delete(place)
        }
    }
}
