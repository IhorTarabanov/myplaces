//
//  NewPlaceTableViewController.swift
//  MyPlaces
//
//  Created by Tarabanov Igor on 24.03.2020.
//  Copyright © 2020 Tarabanov Igor. All rights reserved.
//

import UIKit

class NewPlaceTableViewController: UITableViewController {
    
    var currentPlace: Place!
    
    var imageIsChanged = false
    
    @IBOutlet weak var imageOfPlace: UIImageView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var namePlace: UITextField!
    @IBOutlet weak var lacationPlace: UITextField!
    @IBOutlet weak var typePlace: UITextField!
    @IBOutlet weak var raitingControl: RatingControl!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView(frame: CGRect(x: 0,
                                                         y: 0,
                                                         width: tableView.frame.size.width,
                                                         height: 1))
        
        saveButton.isEnabled = false
        namePlace.addTarget(self, action: #selector(textFieldChanched), for: .editingChanged)
        setupEditScreen()

    }
    //MARK: TableView delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let cameraImage = #imageLiteral(resourceName: "camera")
            let photoImage = #imageLiteral(resourceName: "photo")
            
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let camera = UIAlertAction(title: "Camera", style: .default) { (_) in
                self.choseImagePicker(source: .camera)
            }
            camera.setValue(cameraImage, forKey: "image")
            camera.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            
            let photo = UIAlertAction(title: "Photo", style: .default) { (_) in
                self.choseImagePicker(source: .photoLibrary)
            }
            photo.setValue(photoImage, forKey: "image")
            photo.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
            
            let cansel = UIAlertAction(title: "Cansel", style: .cancel)
            actionSheet.addAction(camera)
            actionSheet.addAction(photo)
            actionSheet.addAction(cansel)
            
            present(actionSheet, animated: true)
        }else{
            view.endEditing(true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier, let mapVC = segue.destination as? MapViewController else {return}
        mapVC.incomeSegueIdentifier = identifier
        mapVC.mapViewControllerDelegate = self
        if identifier == "showPlace"{
            mapVC.place.name = namePlace.text!
            mapVC.place.location = lacationPlace.text
            mapVC.place.type = typePlace.text
            mapVC.place.imageData = imageOfPlace.image?.pngData()
        }
    }
    
    func savePlace(){
        
        let image = imageIsChanged ? imageOfPlace.image : #imageLiteral(resourceName: "imagePlaceholder")
        let imageData = image?.pngData()
        
        let newPlace = Place(name: namePlace.text!,
                             location: lacationPlace.text,
                             type: typePlace.text,
                             imageData: imageData,
                             rating: Double(raitingControl.rating))
        
        if currentPlace != nil {
            try! realm.write{
                currentPlace?.name = newPlace.name
                currentPlace?.location = newPlace.location
                currentPlace?.type = newPlace.type
                currentPlace?.imageData = newPlace.imageData
                currentPlace?.rating = newPlace.rating
            }
        }else{
            StorageManager.saveObject(newPlace)
        }
        
    }
    
    private func setupEditScreen(){
        if currentPlace != nil {
            
            setupNavigationBar()
            imageIsChanged = true
            
            guard let data = currentPlace?.imageData, let image = UIImage(data: data) else {return}//приводим тип Data  к типу UIImage
            imageOfPlace.image = image
            imageOfPlace.contentMode = .scaleAspectFill
            namePlace.text = currentPlace?.name
            lacationPlace.text = currentPlace?.location
            typePlace.text = currentPlace?.type
            raitingControl.rating = Int(currentPlace.rating)
        }
    }
    
    private func setupNavigationBar(){
        if let topItem = navigationController?.navigationBar.topItem{
            topItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        }
        navigationItem.leftBarButtonItem = nil
        title = currentPlace?.name
        saveButton.isEnabled = true
        
    }
    
    @IBAction func dismissNewPlaceView(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true)
    }
    
}
    //MARK: TextField delegate
extension NewPlaceTableViewController: UITextFieldDelegate {
    //прячем клавиатуру
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @objc private func textFieldChanched(){
        if namePlace.text?.isEmpty == false {
            saveButton.isEnabled = true
        }else{
            saveButton.isEnabled = false
        }
    }
}

    //MARK: Work with image
extension NewPlaceTableViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func choseImagePicker(source: UIImagePickerController.SourceType){
        
        if UIImagePickerController.isSourceTypeAvailable(source){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            imagePicker.sourceType = source
            present(imagePicker, animated: true)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        imageOfPlace.image = info[.editedImage] as? UIImage
        imageOfPlace.contentMode = .scaleAspectFill
        imageOfPlace.clipsToBounds = true
        
        imageIsChanged = true
        
        dismiss(animated: true)
    }
}

extension NewPlaceTableViewController: MapViewControllerDelegate {
    func getAddress(_ address: String?) {
        lacationPlace.text = address
    }
    
    
}
